/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Product(name, price, description, color, height) {
    this.name = name;
    this.price = price;
    this.description = description;
    this.color = color;
    this.height = height;
}
var product = new Product("Ressort", "Jouet à mouvement perpétuel", "Bleu", "50cm", "5€");
console.log(product);
var monObjet = JSON.stringify(product);
console.log(monObjet);

var truc = "{\"Name\":\"Miette\",\"jouet\":[{\"Name\":\"Biscuit\"}]}";
console.log(truc);
var leTruc = JSON.parse(truc);
console.log(leTruc);