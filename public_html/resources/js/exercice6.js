function Product(name, price, description, color, height) {
    this.name = name;
    this.price = price;
    this.description = description;
    this.color = color;
    this.height = height;
}

var product = new Product("Ressort", "Jouet à mouvement perpétuel", "Bleu", "50cm", "5€");
var jouet = new Product("Peluche", "4€", "Imitation d'animal sauvage", "Tacheté", "45cm");
function fiche(obj) {
    if (obj instanceof Product)/*instanceof=typeof pour les objets*/ {
        document.getElementById("nomObjet").innerHTML = obj.name;
        document.getElementById("descObjet").innerHTML = obj.price;
        document.getElementById("colObjet").innerHTML = obj.description;
        document.getElementById("longObjet").innerHTML = obj.color;
        document.getElementById("priObjet").innerHTML = obj.height;
    }
}

/*function suivant() {
 if (document.getElementById("nomObjet").innerHTML === "Ressort") {
 fiche(jouet);
 }else{
 fiche(product);
 }
 }
 function précédant() {
 if (document.getElementById("nomObjet").innerHTML === "Peluche") {
 fiche(product);
 }else{
 fiche(jouet);
 }
 }
 fiche(product);*/

var tab=[product, jouet];
    var i=0;
function next() {
   // if (document.getElementById("nomObjet").innerHTML === tab[i].name) {
        fiche(tab[i+1]);
        i++;
   // }
  if (i===tab.length){
   i=0;
}
}
function previous() {
    if (document.getElementById("nomObjet").innerHTML !== tab[i].name) {
        fiche((tab.length)-1);
    }
}
fiche(product);