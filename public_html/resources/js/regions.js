"use strict";
        var generateDepartmentList = new Array("Ain", "Aisne", "Allier", "Alpes-de-Haute-Provence", "Alpes-Maritimes", "Ardèche", "Ardennes", "Ariège", "Aube", "Aude", "Aveyron", "Bas-Rhin", "Bouches-du-Rhône", "Calvados", "Cantal", "Charente", "Charente-Maritime", "Cher", "Corrèze", "Corse-du-Sud", "Côte-d'Or", "Côtes-d'Armor", "Creuse", "Deux-Sèvres", "Dordogne", "Doubs", "Drôme", "Essonne", "Eure", "Eure-et-Loir", "Finistère", "Gard", "Gers", "Gironde", "Guadeloupe", "Guyane", "Haut-Rhin", "Haute-Corse", "Haute-Garonne", "Haute-Loire", "Haute-Marne", "Haute-Saône", "Haute-Savoie", "Haute-Vienne", "Hautes-Alpes", "Hautes-Pyrénées", "Hauts-de-Seine", "Hérault", "Ille-et-Vilaine", "Indre", "Indre-et-Loire", "Isère", "Jura", "La Réunion", "Landes", "Loir-et-Cher", "Loire", "Loire-Atlantique", "Loiret", "Lot", "Lot-et-Garonne", "Lozère", "Maine-et-Loire", "Manche", "Marne", "Martinique", "Mayenne", "Mayotte", "Meurthe-et-Moselle", "Meuse", "Morbihan", "Moselle", "Nièvre", "Nord", "Oise", "Orne", "Paris", "Pas-de-Calais", "Puy-de-Dôme", "Pyrénées-Atlantiques", "Pyrénées-Orientales", "Rhône", "Saône-et-Loire", "Sarthe", "Savoie", "Seine-et-Marne", "Seine-Maritime", "Seine-Saint-Denis", "Somme", "Tarn", "Tarn-et-Garonne", "Territoire de Belfort", "Val-d'Oise", "Val-de-Marne", "Var", "Vaucluse", "Vendée", "Vienne", "Vosges", "Yonne", "Yvelines");
        var d = document.departmentListForm.departmentList;
        for (var i = 0; i < generateDepartmentList.length; i++) {
d.length++;
        d.options[d.length - 1].text = generateDepartmentList[i];
}

function getRegion(department){
    var region;
        if ((department ==='Ain') || (department ==='Allier') || (department ==='Ardèche') || (department ==='Cantal') || (department ==='Drôme') || (department ==='Isère') || (department ==='Loire') || (department ==='Haute-Loire') || (department ==='Puy-de-Dôme') || (department ==='Rhône') || (department ==='Savoie') || (department ==='Haute-Savoie')){
              region = 'Auvergne-Rhône-Alpes';
}else if(department === ('Côte-d\'Or' || 'Doubs' || 'Jura' || 'Nièvre' || 'Haute-Saône' || 'Saône-et-Loire' || 'Yonne' || 'Territoire de Belfort')){
              region = 'Bourgogne-Franche-Comté';
}else if(department === ("Côtes-d\Armor" || "Finistère" || 'Ille-et-Vilaine' || 'Morbihan')){
              region = 'Bretagne';
}else if(department === ('Cher' || 'Eure-et-Loir' || 'Indre' || 'Indre-et-Loire' || 'Loir-et-Cher' || 'Loiret')){
              region = 'Centre-Val-de-Loire';
              }else if(department === ('Corse-du-Sud' || 'Haute-Corse')){
                  region = 'Corse';
              }else if(department === ('Ardennes' || 'Aube' || 'Marne' || 'Haute-Marne' || 'Meurthe-et-Moselle' || 'Meuse' || 'Moselle' || 'Bas-Rhin' || 'Haut-Rhin' || 'Vosges')){
                 region = 'Grand Est';    
}else if(department === ('Aisne' || 'Nord' || 'Oise' || 'Pas-de-Calais' || 'Somme')){
                 region = 'Hauts-de-France';  
}else if(department === ('Paris' || 'Seine-et-Marne' || 'Yvelines' || 'Essonne' || 'Hauts-de-Seine' || 'Seine-Saint-Denis' || 'Val-de-Marne' || 'Val-d\'Oise' || 'Haut-Rhin' || 'Vosges')){
                 region = 'Île de France';
                 }else if(department === ('Calvados' || 'Eure' || 'Manche' || 'Orne' || 'Seine-Maritime')){
                 region = 'Normandie';
             }else if(department === ('Charente' || 'Charente-Maritime' || 'Corrèze' || 'Creuse' || 'Dordogne' || 'Gironde' || 'landes' || 'Lot-et-Garonne' || 'Pyrénées-Atlantiques' || 'Deux-Sèvres' || 'Vienne' || 'Haute-Vienne')){
                 region = 'Nouvelle Aquitaine';  
             }else if(department === ('Ariège' || 'Aude' || 'Aveyron' || 'Gard' || 'Haute-Garonne' || 'Gers'|| 'Hérault' || 'Lot' || 'Lozère' || 'Hautes-Pyrénées' || 'Pyrénées-Orientales' || 'Tarn' || 'Tarn-et-Garonne')){
                 region = 'Occitanie';  
             }else if(department === ('Loire-Atlantique' || 'Maine-et-Loire' || 'Mayenne' || 'Sarthe' || 'Vendée')){
                 region = 'Pays de la Loire';  
             }else if(department === ('Alpes-de-Haute-Provence' || 'Hautes-Alpes' || 'Alpes-Maritimes' || 'Bouches-du-Rhône' || 'Vaucluse')){
                 region = 'Provence-Alpes-Côte d\'Azur';}
document.getElementById("resultat").innerHTML = "Votre département appartient à la region " + region;
}