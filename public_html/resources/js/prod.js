/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
"use strict";

/**
 * Constructor of Product
 * @param {type} _id
 * @param {type} _name
 * @param {type} _desc
 * @param {type} _price
 * @returns {Product}
 */
function Product(_id, _name, _desc, _price) {
    this.id = _id;
    this.name = _name;
    this.desc = _desc;
    this.price = _price;
}
/**
 * Instanciation of three products
 * @type Product
 */
var p1 = new Product(0, "Mon premier produit", "Premier objet créé", 10);
var p2 = new Product(1, "Mon second produit", "Second objet créé", 15);
var p3 = new Product(2, "Mon troisieme produit", "Troisieme objet créé", 19);

/**
 * Function to display one product (prod) into HTML page
 * @param {type} prod
 * @returns {undefined}
 */
function displayProduct(prod) {
    if (prod instanceof Product) {
        document.getElementById("id").innerHTML = prod.id;
        document.getElementById("name").innerHTML = prod.name;
        document.getElementById("desc").innerHTML = prod.desc;
        document.getElementById("price").innerHTML = prod.price;
    } else {
        alert("Vous ne souhaitez pas afficher un produit");
    }
}
/**
 * Initialisation of HTML page displaying one product, here p1
 * @param Product
 */
displayProduct(p1);

/**
 * Initialisation of products tab with all products instanciated before
 * @type Array
 */
var products = [p2, p1, p3];


/**
 * Function to display next product
 * @returns {undefined}
 */
function getNextProduct() {
    /*get id of product displaying in HTML page*/
    var currentId = parseInt(document.getElementById("id").innerHTML);
    /*get index of this product in array products*/
    var currentIndex = getIndexFromProductsTab(currentId, products);
    /*if the product displaying is the last of the array so nextIndex = 0*/
    if (currentIndex === (products.length - 1)) {
        var nextIndex = 0;
    } else {
        /*else nextIndex = currentIndex + 1*/
        var nextIndex = currentIndex + 1;
    }
    /* display product thanks to function displayProduct*/
    /*param is object in index nextIndex into products array*/
    displayProduct(products[nextIndex]);
}

/**
 * Function to display previous product
 * @returns {undefined}
 */
function getPreviousProduct() {
    var currentId = parseInt(document.getElementById("id").innerHTML);
    var currentIndex = getIndexFromProductsTab(currentId, products);
    if (currentIndex === 0) {
        var nextIndex = products.length - 1;
    } else {
        var nextIndex = currentIndex - 1;
    }
    
    displayProduct(products[nextIndex]);
}

/**
 * Function to get index of element in tab
 * @param {type} search : currentId = id of product displaying in HTML page
 * @param {type} tab : array when we must search element
 * @returns {number} : index
 */
function getIndexFromProductsTab(search, tab){
    return tab.findIndex(function (element) { /* findIndex : js function*/
        if (element instanceof Product) {
            if (element.id === search) { /*if id of element equals id searched so...*/
                return element;
            }
        }
    });
}



